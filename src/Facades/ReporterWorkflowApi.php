<?php

namespace Pavlosof\ReporterWorkflowApi\Facades;

use Illuminate\Support\Facades\Facade;

class ReporterWorkflowApi extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'reporterworkflowapi';
    }
}
