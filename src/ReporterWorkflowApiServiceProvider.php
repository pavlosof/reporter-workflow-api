<?php

namespace Pavlosof\ReporterWorkflowApi;

use Illuminate\Support\ServiceProvider;

class ReporterWorkflowApiServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../app/' => app_path() . '/',
        ]);
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');


        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/reporterworkflowapi.php', 'reporterworkflowapi');

        // Register the service the package provides.
        $this->app->singleton('reporterworkflowapi', function ($app) {
            return new ReporterWorkflowApi;
        });
        include __DIR__.'/routes/api.php';
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['reporterworkflowapi'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/reporterworkflowapi.php' => config_path('reporterworkflowapi.php'),
        ], 'reporterworkflowapi.config');
    }
}
